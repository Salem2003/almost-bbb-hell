extends CanvasLayer

var entrar1: bool = true
var entrar2: bool = true
var entrar3: bool = true
var vida_extra: bool = false
var random = RandomNumberGenerator.new()
var num_carta1
var num_carta2
var num_carta3

var curacion: bool = false

func _process(_delta):
	if get_parent().carta:
		print(vida_extra)
		%"Cartas Jugador".visible = true
		if entrar1:
			entrar1 = false
			num_carta1 = random.randi_range(0, 8)
			if num_carta1 == 1 && !vida_extra:
				carta_vida_extra()
			num_carta1 = random.randi_range(0, 8)
			if num_carta1 == 7 && !curacion:
				%Curacion.visible = true
				curacion = true
		
		if entrar2:
			entrar2 = false
			num_carta2 = random.randi_range(0, 8)
			if num_carta2 == 1 && !vida_extra:
				carta_vida_extra()
			num_carta2 = random.randi_range(0, 8)
			if num_carta2 == 7 && !curacion:
				%Curacion.visible = true
				curacion = true
		
		if entrar3:
			entrar3 = false
			num_carta3 = random.randi_range(0, 8)
			if num_carta3 == 1 && !vida_extra:
				carta_vida_extra()
			num_carta3 = random.randi_range(0, 8)
			if num_carta3 == 7 && !curacion:
				%Curacion.visible = true
				curacion = true
	#if get_parent().carta:
	#	%"Cartas Jugador".visible = true
	#	if entrar1:
	#		entrar1 = false
	#		num_carta1 = random.randi_range(0, 8)
	#		if num_carta1 >= 1 && num_carta1 <= 6:
	#			%"Aumento ataque".visible = true
	#		elif num_carta1 == 0:
	#			if !vida_extra:
	#				carta_vida_extra()
	#			else:
	#				%"Aumento ataque".visible = true
	#		elif num_carta1 >= 7 && num_carta1 <= 8:
	#			carta_curacion()
	#		#print(num_carta1)
	#		if %"Aumento ataque".visible == false && %"Vida extra".visible == false && %Curacion.visible == false:
	#			%"Aumento ataque".visible = true
	#	if entrar2:
	#		entrar2 = false
	#		num_carta2 = random.randi_range(0, 8)
	#		if num_carta2 >= 1 && num_carta2 <= 6:
	#			%"Aumento vida".visible = true
	#		elif num_carta2 == 0:
	#			if !vida_extra:
	#				carta_vida_extra()
	#			else:
	#				%"Aumento vida".visible = true
	#		elif num_carta2 >= 7 && num_carta2 <= 8:
	#			carta_curacion()
	#		if %"Aumento vida".visible == false && %"Vida extra".visible == false && %Curacion.visible == false:
	#			%"Aumento vida".visible = true
	#	if entrar3:
	#		entrar3 = false
	#		num_carta3 = random.randi_range(0, 8)
	#		if num_carta3 >= 7 && num_carta3 <= 8:
	#			carta_curacion()
	#		elif num_carta3 >= 1 && num_carta3 <= 6:
	#			%"Aumento vel disparo".visible = true
	#		elif num_carta3 == 0:
	#			if !vida_extra:
	#				carta_vida_extra()
	#			else:
	#				%"Aumento vel disparo".visible = true
	#		if %"Aumento vel disparo".visible == false && %"Vida extra".visible == false && %Curacion.visible == false:
	#			%"Aumento vel disparo".visible = true

func _on_aumento_ataque_pressed():
	%Jugador.daño_proyectil += 2
	get_parent().carta = false
	Muerto()
	Invisibilizar()


func _on_vida_extra_pressed():
	vida_extra = true
	%"Vida extra".visible = false
	%Jugador.revivir = true
	get_parent().carta = false
	Muerto()
	Invisibilizar()


func _on_aumento_vida_pressed():
	%Jugador.vida_max += 30
	%Jugador.get_child(3).max_value = %Jugador.vida_max
	get_parent().carta = false
	Muerto()
	Invisibilizar()


func _on_curacion_pressed():
	if (%Jugador.vida + 70) >= %Jugador.vida_max:
		%Jugador.vida = %Jugador.vida_max
	else:
		%Jugador.vida += 70
	%Jugador.get_child(3).value = %Jugador.vida_max
	get_parent().carta = false
	Muerto()
	Invisibilizar()


func _on_aumento_vel_disparo_pressed():
	%Jugador.get_child(2).wait_time -= 0.05
	get_parent().carta = false
	Muerto()
	Invisibilizar()


func Invisibilizar():
	entrar1 = true
	entrar2 = true
	entrar3 = true
	#%"Aumento ataque".visible = false
	#%"Vida extra".visible = false
	#%"Aumento vida".visible = false
	#%Curacion.visible = false
	#%"Aumento vel disparo".visible = false
	%"Cartas Jugador".visible = false
	%"Aumento ataque".visible = true
	%"Aumento vida".visible = true
	%"Aumento vel disparo".visible = true
	%"Vida extra".visible = false
	%Curacion.visible = false
	curacion = false

func carta_vida_extra():
		%"Vida extra".visible = true

func carta_curacion():
	%Curacion.visible = true

func Muerto():
	if get_parent().jefe_actual < get_parent().ultimo_jefe:
		get_parent().muerto = true
	else:
		%"Tiempo pre warning".paused = false
