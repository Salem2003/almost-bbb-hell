extends ParallaxBackground

@export var direccion = Vector2(0, 0)
@export var velocidad: int = 0

func _physics_process(delta):
	scroll_base_offset -= direccion * velocidad * delta
