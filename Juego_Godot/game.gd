extends Node2D

const HÜDRA = preload("res://Assets/Bosses/Hüdra/Hüdra.tscn")
const AHJU = preload("res://Assets/Bosses/Ahju/Ahju.tscn")
const NAUTILUS = preload("res://Assets/Bosses/Nautilus/Nautilus.tscn")

const VALGUS = preload("res://Assets/Bosses/Valgus/Valgus.tscn")

var jefe
var jefe_seleccionado
var num_jefe
var lista_jefes = [HÜDRA, AHJU, NAUTILUS, false, false, false]

var cantidad = lista_jefes.size() / 2.0

var velocidad_movimiento: int = 100

var muerto: bool = true
var entrar: bool = true
var randomizar: bool = true
var disparo: bool = false
var carta: bool = false
var noche: bool = false
var anochecer: int
var ganar: bool = false

@export var ultimo_jefe: int
var jefe_actual: int = 0

var random = RandomNumberGenerator.new()

func _ready():
	$AudioManager.music.play()
	$AudioManager.layer.play()
	%"Tiempo warning".paused = true
	%"Tiempo pre warning".paused = true

func _physics_process(delta):
	if ganar:
		%"You Win".visible = true
		get_tree().paused = true
		$AudioManager.you_win.play()
		#%Jugador.queue_free()
	
	if !noche:
		anochecer = random.randi_range(0, 9)
		if anochecer <= 2:
			$Noche.visible = true
		noche = true
	if Input.is_action_just_pressed("Pausar"):
		get_tree().paused = true
		$Pausa.visible = true
	
	if muerto:
		if randomizar:
			randomizar = false
			num_jefe = 0
			num_jefe = random.randi_range(0, 2)
			if jefe_actual < ultimo_jefe:
				if !lista_jefes[num_jefe + cantidad]:
					jefe_seleccionado = lista_jefes[num_jefe]
					lista_jefes[num_jefe + cantidad] = true
					entrar = true
				else:
					randomizar = true
			elif jefe_actual == ultimo_jefe:
				entrar = true
				jefe_seleccionado = VALGUS
		if entrar:
			if jefe_actual == ultimo_jefe:
				$AudioManager.layer.volume_db = -5.0
			jefe = jefe_seleccionado.instantiate()
			%Jefe.add_child(jefe)
			jefe.global_position.y = -2500 #-310 #1000
			entrar = false
		if jefe != null:
			if jefe.global_position.y < -311:
				jefe.global_position.y += 150 * delta
				if jefe.global_position.y >= -310:
					muerto = false
					disparo = true
					jefe_actual += 1
				jefe.global_position.y += velocidad_movimiento * delta

func _on_jugador_sin_vida():
	%"Game over".visible = true
	get_tree().paused = true
	$AudioManager.game_over.play()
	%Jugador.queue_free()


func _on_salir_pressed():
	get_tree().quit()


func _on_continuar_pressed():
	get_tree().paused = false
	$Pausa.visible = false


func _on_tiempo_warning_timeout():
	%"Tiempo warning".paused = true
	%Warning.visible = false
	%LineaWarning.visible = false
	%LineaWarning2.visible = false
	muerto = true


func _on_tiempo_pre_warning_timeout():
	%"Tiempo warning".paused = false
	%"Tiempo pre warning".paused = true
	%Warning.visible = true
	%LineaWarning.visible = true
	%LineaWarning2.visible = true
