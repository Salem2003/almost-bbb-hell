extends Area2D

@export var velocidad_proyectil = 0
var distancia_recorrida = 0
var distancia_limite = 2000
@export var daño = 0

func _physics_process(delta):
	var direccion = Vector2.RIGHT.rotated(rotation) #hace q los proyectiles salgan disparados con la rotacion correcta
	position += direccion * velocidad_proyectil * delta
	distancia_recorrida += velocidad_proyectil * delta
	
	if distancia_recorrida >= distancia_limite:
		queue_free() #destruye el objeto

func _on_body_entered(body):
	if body.has_method("recibir_daño"):
		body.recibir_daño(daño)
	queue_free()
