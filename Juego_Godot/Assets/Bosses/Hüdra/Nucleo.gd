extends CharacterBody2D

@export var vida: int
var laser: bool = false
var random = RandomNumberGenerator.new()
var num: int = 0
@onready var cura = preload("res://Assets/PJ/cura.tscn")
var curacion

func _ready():
	%ProgressBar.visible = false
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida

func _physics_process(_delta):
	if laser:
		%ProgressBar.visible = true

func recibir_daño(daño):
	vida -= daño
	%ProgressBar.value = vida
	if vida <= 0:
		num = random.randi_range(0, 6)
		if num == 1:
			curacion = cura.instantiate()
			add_child(curacion)
			curacion.global_position = global_position
