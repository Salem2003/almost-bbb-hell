extends Marker2D

var resultado_resta_de_vectores = Vector2()
var proyectiles_disparados: int = 0
var rafaga: bool = false

var entrar: bool = false
var entrar2: bool = true

const BALA = preload("res://Assets/Bosses/Hüdra/proyectil_jefe_1_grande.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala
#@export var disparador: Marker2D

func _ready():
	$"../../Velocidad disparo mas rapido".paused = true
	$"../../Velocidad disparo rapido".paused = true
	$"../../Velocidad disparo".paused = true

func _physics_process(_delta):
	if get_parent().get_parent().get_parent().get_parent().get_parent().disparo && entrar2:
		#$"../../Velocidad disparo rapido".paused = false
		$"../../Velocidad disparo".paused = false
		entrar2 = false
	
	if !entrar && get_parent().get_parent().rapido:
		$"../../Velocidad disparo mas rapido".paused = false
		$"../../Velocidad disparo".paused = true

func disparar():
	bala = BALA.instantiate()
	add_child(bala)
	bala.global_position = global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	if !get_parent().get_parent().rapido:
		%AudioManager.super_cañon.play()
		disparar()
		proyectiles_disparados = 0
		rafaga = true
		$"../../Velocidad disparo".paused = true
		$"../../Velocidad disparo rapido".paused = false


func _on_velocidad_disparo_rapido_timeout():
	if proyectiles_disparados < 5 && rafaga:
		if proyectiles_disparados == 4:
			rafaga = false
			$"../../Velocidad disparo rapido".paused = true
			if get_parent().get_parent().rapido:
				$"../../Velocidad disparo mas rapido".paused = false
			elif !get_parent().get_parent().rapido:
				$"../../Velocidad disparo".paused = false
		proyectiles_disparados += 1
		%AudioManager.super_cañon.play()
		disparar()


func _on_velocidad_disparo_mas_rapido_timeout():
	if get_parent().get_parent().rapido:
		%AudioManager.super_cañon.play()
		disparar()
		proyectiles_disparados = 0
		rafaga = true
		$"../../Velocidad disparo mas rapido".paused = true
		$"../../Velocidad disparo rapido".paused = false
