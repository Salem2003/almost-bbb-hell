extends CharacterBody2D

var vida = 150
var sujeto = 0
var random = RandomNumberGenerator.new()
var num: int = 0
@onready var cura = preload("res://Assets/PJ/cura.tscn")
var curacion

func _ready():
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida

func recibir_daño(daño):
	vida -= daño
	%ProgressBar.value = vida
	if vida <= 100 && sujeto == 0:
		SpawnearCura()
		$"Mascara1".queue_free()
		sujeto += 1
	if vida <= 50 && sujeto == 1:
		SpawnearCura()
		$"Mascara2".queue_free()
		sujeto += 1
	if vida <= 0 && sujeto == 2:
		SpawnearCura()
		$Mascara3.queue_free()
		%AudioManager.metal.play()
		$"Colision mascara".queue_free()
		%ProgressBar.queue_free()

func SpawnearCura():
	num = random.randi_range(0, 9)
	if num == 1:
		curacion = cura.instantiate()
		add_child(curacion)
		curacion.global_position = global_position
