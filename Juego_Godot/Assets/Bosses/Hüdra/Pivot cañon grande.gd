extends Node2D

var objetivo
var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion = 1.5
var entrar: bool = true

const BALA_DERECHA = preload("res://Assets/Bosses/Hüdra/bala_jefe_1_derecha.tscn") #la convencion dice q las constantes se escriben en mayusculas
const BALA_IZQUIERDA = preload("res://Assets/Bosses/Hüdra/bala_jefe_1_derecha.tscn")

func _ready():
	objetivo = get_node("/root/Game/PJ/Jugador")
	$"Velocidad disparo".paused = true

func _physics_process(delta):
	if get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		$"Velocidad disparo".paused = false
		entrar = false
	resultado_resta_de_vectores = objetivo.position - global_position #esta resta da como resultado la posicion del PJ
	rotation = lerp_angle(rotation, atan2(resultado_resta_de_vectores.y, resultado_resta_de_vectores.x), delta * velocidad_rotacion)

func disparar():
	var bala_derecha = BALA_DERECHA.instantiate()
	var bala_izquierda = BALA_IZQUIERDA.instantiate()
	
	%Disparador.add_child(bala_derecha)
	%Disparador2.add_child(bala_izquierda)
	bala_derecha.position = %Disparador2.global_position
	bala_derecha.rotation = %Disparador2.global_rotation
	#get_node("/root/Game/Jefe 1/Cañon grande/Colision cañon grande/Pivot cañon grande/Disparador").add_child(bala_derecha)
	#get_node("/root/Game").add_child(bala_derecha)
	bala_izquierda.global_position = %Disparador.global_position
	bala_izquierda.global_rotation = %Disparador2.global_rotation
	#get_node("/root/Game/Jefe 1/Cañon grande/Colision cañon grande/Pivot cañon grande/Disparador2").add_child(bala_izquierda)
	#get_node("/root/Game").add_child(bala_izquierda)

func _on_velocidad_disparo_timeout():
	%AudioManager.bala_grande.play()
	disparar()
