extends Marker2D

var disparar_rayo: bool = false
var rayo_activo: bool = false
var num_temporizador: int = 0
var entrar: bool = false

const AVISO = preload("res://Assets/Bosses/Hüdra/aviso_rayo.tscn")
var aviso
const RAYO = preload("res://Assets/Bosses/Hüdra//rayo_jefe_1.tscn")
var rayo
#@export var disparador: Marker2D

func _ready():
	%"Velocidad aviso".paused = true
	%"Teimpo permanencia aviso".paused = true
	%"Tiempo permanencia rayo".paused = true
	num_temporizador = 1

func _physics_process(_delta):
	if !entrar && get_parent().laser:
		entrar = true
		%"Velocidad aviso".paused = false

func disparar():
	aviso = AVISO.instantiate()
	rayo = RAYO.instantiate()
	
	if !disparar_rayo:
		add_child(aviso)
		aviso.global_position = global_position
		aviso.global_rotation = global_rotation
	if disparar_rayo:
		add_child(rayo)
		rayo.global_position = global_position
		rayo.global_rotation = global_rotation

func _on_teimpo_permanencia_aviso_timeout():
	#if rayo_activo && get_parent().get_parent().laser:
	#	disparar_rayo = false
	#	rayo.queue_free()
	if num_temporizador == 2 && get_parent().laser:
		aviso.queue_free()
		disparar_rayo = true
		%AudioManager.rayo.play()
		disparar()
		%"Tiempo permanencia rayo".paused = false
		%"Teimpo permanencia aviso".paused = true
		num_temporizador = 3

func _on_velocidad_aviso_timeout():
	#if get_parent().get_parent().laser:
	#	disparar()
	#	disparar_rayo = true
	#	rayo_activo = true
	#pass
	if num_temporizador == 1 && get_parent().laser:
		disparar_rayo = false
		disparar()
		%"Velocidad aviso".paused = true
		%"Teimpo permanencia aviso".paused = false
		num_temporizador = 2

func _on_tiempo_permanencia_rayo_timeout():
	if num_temporizador == 3 && get_parent().laser:
		rayo.queue_free()
		num_temporizador = 1
		%"Tiempo permanencia rayo".paused = true
		%"Velocidad aviso".paused = false
