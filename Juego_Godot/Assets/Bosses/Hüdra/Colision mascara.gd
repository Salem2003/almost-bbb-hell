extends CollisionShape2D

var vida = 30

func gritar():
	print("AHHHHHHHHHH")

func recibir_daño(daño):
	vida -= daño
	gritar()
	if vida <= 20:
		get_node("Mascara1").queue_free()
	if vida <= 10:
		get_node("Mascara2").queue_free()
	if vida <= 0:
		queue_free()
