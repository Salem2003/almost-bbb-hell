extends Node2D

var objetivo
var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion: int
var proyectiles_disparados: int = 0
var rafaga: bool = false
var entrar: bool = true

const BALA = preload("res://Assets/Bosses/Hüdra/proyectil_jefe_1_mediano.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala

func _ready():
	objetivo = get_node("/root/Game/PJ/Jugador")
	%"Velocidad disparo".paused = true
	$"Velocidad disparo rapido".paused = true

func _physics_process(delta):
	if get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		%"Velocidad disparo".paused = false
		$"Velocidad disparo rapido".paused = false
		entrar = false
	
	resultado_resta_de_vectores = objetivo.position - global_position #esta resta da como resultado la posicion del PJ
	rotation = lerp_angle(rotation, atan2(resultado_resta_de_vectores.y, resultado_resta_de_vectores.x), delta * velocidad_rotacion)

func disparar():
	bala = BALA.instantiate()
	%Disparador.add_child(bala)
	bala.global_position = %Disparador.global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	%AudioManager.proyectil_mediano.play()
	disparar()
	proyectiles_disparados = 0
	rafaga = true


func _on_velocidad_disparo_rapido_timeout():
	if proyectiles_disparados < 3 && rafaga:
		if proyectiles_disparados == 2:
			rafaga = false
		proyectiles_disparados += 1
		%AudioManager.proyectil_mediano.play()
		disparar()
