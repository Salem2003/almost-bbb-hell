extends CharacterBody2D

var vida = 225
var sujeto = 0
var random = RandomNumberGenerator.new()
var num: int = 0
@onready var cura = preload("res://Assets/PJ/cura.tscn")
var curacion

func _ready():
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida

func recibir_daño(daño):
	vida -= daño
	%ProgressBar.value = vida
	if vida <= 150 && sujeto == 0:
		SpawnearCura()
		$Mascara1.queue_free()
		sujeto += 1
	if vida <= 75 && sujeto == 1:
		SpawnearCura()
		$Mascara2.queue_free()
		sujeto += 1
	if vida <= 0 && sujeto == 2:
		SpawnearCura()
		$Mascara3.queue_free()
		%AudioManager.mascara_rota_valgus.play()
		$CollisionShape2D.queue_free()
		%ProgressBar.queue_free()
		get_parent().rapido = true

func SpawnearCura():
	num = random.randi_range(0, 9)
	if num == 1:
		curacion = cura.instantiate()
		add_child(curacion)
		curacion.global_position = global_position
