extends Marker2D

var entrar: bool = true

const BALA = preload("res://Assets/Bosses/Valgus/estrella.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala

func _ready():
	%"Velocidad disparo".paused = true

func _physics_process(_delta):
	if get_parent().get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		%"Velocidad disparo".paused = false
		entrar = false

func disparar():
	bala = BALA.instantiate()
	%Disparador.add_child(bala)
	bala.global_position = %Disparador.global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	%AudioManager.estrella.play()
	disparar()
