extends Area2D

@export var daño = 0

func _ready():
	%AudioManager.explosion.play()

func _on_body_entered(body):
	if body.has_method("recibir_daño"):
		body.recibir_daño(daño)

func _on_tiempo_explosion_timeout():
	queue_free()
