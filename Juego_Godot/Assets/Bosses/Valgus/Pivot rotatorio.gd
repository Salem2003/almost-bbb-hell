extends Node2D

var horario: bool = true
@export var velocidad_rotacion: float

func _physics_process(delta):
	if horario:
		rotation -= velocidad_rotacion * delta
	if horario == false:
		rotation += velocidad_rotacion * delta


func _on_rotacion_horario_timeout():
	if !horario:
		horario = true
	$"Rotacion horario".paused = true
	$"Rotacion antihorario".paused = false


func _on_rotacion_antihorario_timeout():
	if horario:
		horario = false
	$"Rotacion horario".paused = false
	$"Rotacion antihorario".paused = true
