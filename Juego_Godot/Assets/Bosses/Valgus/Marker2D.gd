extends Marker2D

var disparar_rayo: bool = false
var rayo_activo: bool = false
var num_temporizador: int = 0
var entrar: bool = true

const AVISO = preload("res://Assets/Bosses/Valgus/aviso_laser.tscn")
var aviso
const RAYO = preload("res://Assets/Bosses/Valgus/laser.tscn")
var rayo

func _ready():
	%"Velocidad aviso".paused = true
	%"Teimpo permanencia aviso".paused = true
	%"Tiempo permanencia rayo".paused = true
	num_temporizador = 1

func _physics_process(_delta):
	if get_parent().get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		%"Velocidad aviso".paused = false
		entrar = false

func disparar():
	aviso = AVISO.instantiate()
	rayo = RAYO.instantiate()
	
	if !disparar_rayo:
		add_child(aviso)
		aviso.global_position = global_position
		aviso.global_rotation = global_rotation
	if disparar_rayo:
		add_child(rayo)
		rayo.global_position = global_position
		rayo.global_rotation = global_rotation

func _on_teimpo_permanencia_aviso_timeout():
	if num_temporizador == 2:
		aviso.queue_free()
		disparar_rayo = true
		%AudioManager.lasersito.play()
		get_parent().velocidad_rotacion = 0
		disparar()
		%"Tiempo permanencia rayo".paused = false
		%"Teimpo permanencia aviso".paused = true
		num_temporizador = 3

func _on_velocidad_aviso_timeout():
	if num_temporizador == 1:
		disparar_rayo = false
		disparar()
		%"Velocidad aviso".paused = true
		%"Teimpo permanencia aviso".paused = false
		num_temporizador = 2

func _on_tiempo_permanencia_rayo_timeout():
	if num_temporizador == 3:
		rayo.queue_free()
		num_temporizador = 1
		get_parent().velocidad_rotacion = get_parent().vel_rotacion
		%"Tiempo permanencia rayo".paused = true
		%"Velocidad aviso".paused = false
