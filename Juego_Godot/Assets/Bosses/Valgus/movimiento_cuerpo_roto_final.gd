extends CharacterBody2D

var distancia_recorrida = 0
var distancia_limite = 2000
var velocidad_movimiento: int = 300
@export var jefe_roto: CharacterBody2D

func _physics_process(delta):
	position.y += velocidad_movimiento * delta
	distancia_recorrida += velocidad_movimiento * delta
	
	if distancia_recorrida >= distancia_limite:
		get_parent().get_parent().jefe = jefe_roto
		#get_parent().get_parent().muerto = true
		get_parent().get_parent().jefe = null
		get_parent().get_parent().jefe_seleccionado = null
		get_parent().get_parent().ganar = true
		queue_free() #destruye el objeto
