extends Area2D

@export var daño: int

func _on_body_entered(body):
	if body.has_method("recibir_daño"):
		body.recibir_daño(daño)
