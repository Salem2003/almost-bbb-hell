extends Node2D

var velocidad_rotacion: float = 2

func _physics_process(delta):
	if get_parent().rotacion == 1:
		rotation += velocidad_rotacion * delta
	elif get_parent().rotacion == 2:
		rotation -= velocidad_rotacion * delta
