extends Marker2D

var entrar: bool = true
var sonido: bool = true

var proyectiles_disparados: int = 0
var rafaga: bool = false
var pausar: bool = false

const BALA_1 = preload("res://Assets/Bosses/Valgus/proyectil_chico.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala1
const BALA_2 = preload("res://Assets/Bosses/Valgus/proyectil_mediano.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala2

func _ready():
	%"Velocidad disparo".paused = true
	%"Velocidad disparo rapido".paused = true

func _physics_process(_delta):
	if get_parent().get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		%"Velocidad disparo".paused = false
		entrar = false
	
	if get_parent().get_parent().get_parent().rapido:
		%"Velocidad disparo".paused = false
		%"Velocidad disparo rapido". paused = true
		%"Velocidad disparo".wait_time = 0.1
	
	if get_parent().get_parent().get_parent().get_child(1).vida <= 0 && !pausar:
		pausar = true
		%AudioManager.machin_gan.stop()
		%Sonido.start(5)

func disparar():
	if !get_parent().get_parent().get_parent().rapido:
		bala1 = BALA_1.instantiate()
		add_child(bala1)
		bala1.global_position = global_position
		bala1.global_rotation = global_rotation
	elif get_parent().get_parent().get_parent().rapido:
		bala2 = BALA_2.instantiate()
		add_child(bala2)
		bala2.global_position = global_position
		bala2.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	disparar()
	proyectiles_disparados = 0
	rafaga = true
	if !get_parent().get_parent().get_parent().rapido:
		%"Velocidad disparo".paused = true
		%"Velocidad disparo rapido".paused = false
	if sonido:
		%AudioManager.machin_gan.play(0.0)
		sonido = false
		%Sonido.start(30)


func _on_velocidad_disparo_rapido_timeout():
	if proyectiles_disparados < 60 && rafaga:
		if proyectiles_disparados == 59:
			rafaga = false
			%"Velocidad disparo rapido".paused = true
			%"Velocidad disparo".paused = false
			%Sonido.stop()
			%AudioManager.machin_gan.stop()
			sonido = true
		proyectiles_disparados += 1
		disparar()


func _on_sonido_timeout():
	sonido = true
