extends Area2D

#@export var velocidad_proyectil: int
var velocidad: int = 50
@export var daño = 0
@export var reduccion_velocidad: int

const EXPLOSION = preload("res://Assets/Bosses/Valgus/explosion.tscn")
var explosion

func _ready():
	%Quietud.paused = true
	%"Reduccion velocidad".paused = true

func _physics_process(delta):
	var direccion = Vector2.RIGHT.rotated(rotation) #hace q los proyectiles salgan disparados con la rotacion correcta
	position += direccion * velocidad * delta


func _on_body_entered(body):
	if body.has_method("recibir_daño"):
		body.recibir_daño(daño)
	queue_free()


func _on_reduccion_velocidad_timeout():
	velocidad -= reduccion_velocidad
	
	if velocidad <= 0:
		velocidad = 0
		%"Reduccion velocidad".paused = true
		%Quietud.paused = false


func _on_quietud_timeout():
	explosion = EXPLOSION.instantiate()
	get_parent().add_child(explosion)
	explosion.global_position = global_position
	#explosion.global_rotation = global_rotation
	queue_free()
	%Quietud.paused = true


func _on_comienzo_reduccion_timeout():
	%"Comienzo reduccion".paused = true
	%"Reduccion velocidad".paused = false
