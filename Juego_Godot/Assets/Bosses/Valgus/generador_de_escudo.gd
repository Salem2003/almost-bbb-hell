extends CharacterBody2D

@export var rotacion: int
var desaparecer: bool = false

@export var vida = 0
var random = RandomNumberGenerator.new()
var num: int = 0
@onready var cura = preload("res://Assets/PJ/cura.tscn")
var curacion

func _ready():
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida

func recibir_daño(daño):
	vida -= daño
	%ProgressBar.value = vida
	if vida <= 0:
		desaparecer = true
		num = random.randi_range(0, 6)
		if num == 1:
			curacion = cura.instantiate()
			add_child(curacion)
			curacion.global_position = global_position
		$Node2D/GeneradorDeEscudo.visible = false
		%ProgressBar.queue_free()
		$CollisionShape2D.queue_free()
