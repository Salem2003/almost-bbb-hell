extends CharacterBody2D

@export var vida = 0
var entrar: bool = true
var random = RandomNumberGenerator.new()
var num: int = 0
@onready var cura = preload("res://Assets/PJ/cura.tscn")
var curacion

func _ready():
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida
	%"Velocidad disparo".paused = true

func _physics_process(_delta):
	if get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		%"Velocidad disparo".paused = false
		entrar = false

func recibir_daño(daño):
	vida -= daño
	%ProgressBar.value = vida
	if vida <= 0:
		num = random.randi_range(0, 6)
		if num == 1:
			curacion = cura.instantiate()
			add_child(curacion)
			curacion.global_position = global_position
		%Sprite.queue_free()
		%Colision.queue_free()
		%ProgressBar.queue_free()
		%"Velocidad disparo".paused = true
