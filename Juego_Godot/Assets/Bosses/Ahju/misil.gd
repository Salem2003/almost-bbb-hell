extends CharacterBody2D

@export var vida: int
var valocidad
@export var velocidad_proyectil: int
var direccion
var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion: int

var distancia_recorrida = 0
var distancia_limite = 3000

@onready var objetivo = get_node("/root/Game/PJ/Jugador")

func _physics_process(delta):
	resultado_resta_de_vectores = objetivo.position - global_position #esta resta da como resultado la posicion del PJ
	rotation = lerp_angle(rotation, atan2(resultado_resta_de_vectores.y, resultado_resta_de_vectores.x), delta * velocidad_rotacion)
	
	direccion = global_position.direction_to(objetivo.global_position)
	velocity = direccion * velocidad_proyectil
	move_and_slide()
	
	distancia_recorrida += velocidad_proyectil * delta
	if distancia_recorrida >= distancia_limite:
		queue_free()

func recibir_daño(daño):
	vida -= daño
	if vida <= 0:
		queue_free()
