extends CharacterBody2D

@export var vida: int
var entrar: bool = true
var random = RandomNumberGenerator.new()
var num: int = 0
@onready var cura = preload("res://Assets/PJ/cura.tscn")
var curacion

func _ready():
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida
	%ProgressBar. visible = false
	%CollisionShape2D.disabled = true
	%CollisionShape2D2.disabled = true
	%CollisionShape2D3.disabled = true
	$CollisionShape2D4.disabled = true
	$CollisionShape2D5.disabled = true

func _physics_process(_delta):
	if find_child("Cañon de misiles").vida <= 0 && entrar:
		%ProgressBar.visible = true
		%CollisionShape2D.disabled = false
		%CollisionShape2D2.disabled = false
		%CollisionShape2D3.disabled = false
		$CollisionShape2D4.disabled = false
		$CollisionShape2D5.disabled = false
		entrar = false
	#else:
	#	print()

func recibir_daño(daño):
	vida -= daño
	num = random.randi_range(0, 6)
	%ProgressBar.value = vida
	if vida <= 0:
		if num == 1:
			curacion = cura.instantiate()
			add_child(curacion)
			curacion.global_position = global_position
		%Sprite.visible = false
		%ProgressBar.visible = false
		%CollisionShape2D.queue_free()
		%CollisionShape2D2.queue_free()
		%CollisionShape2D3.queue_free()
		%CollisionShape2D4.queue_free()
		%CollisionShape2D5.queue_free()
		%"Velocidad disparo".paused = true
