extends Marker2D

var disparar_rayo: bool = false
var rayo_activo: bool = false
var num_temporizador: int = 0
var entrar: bool = false
var entrar2: bool = true

var objetivo
var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion: float = 0

const AVISO = preload("res://Assets/Bosses/Ahju/aviso_rayo.tscn")
var aviso
const RAYO = preload("res://Assets/Bosses/Ahju/lanzallamas.tscn")
var rayo
#@export var disparador: Marker2D

func _ready():
	objetivo = get_node("/root/Game/PJ/Jugador")
	$"../../Teimpo permanencia aviso".paused = true
	$"../../Tiempo permanencia rayo".paused = true
	$"../../Velocidad aviso".paused = true
	num_temporizador = 1

func _physics_process(delta):
	if get_parent().get_parent().get_parent().get_parent().get_parent().disparo && entrar2:
		$"../../Velocidad aviso".paused = false
		entrar2 = false
	
	resultado_resta_de_vectores = objetivo.position - global_position #esta resta da como resultado la posicion del PJ
	rotation = lerp_angle(rotation, atan2(resultado_resta_de_vectores.y, resultado_resta_de_vectores.x), delta * velocidad_rotacion)
	
	if get_parent().get_parent().laser:
		$"../../Velocidad aviso".wait_time = 7
		$"../../Teimpo permanencia aviso".wait_time = 1
		$"../../Tiempo permanencia rayo".wait_time = 2

func disparar():
	aviso = AVISO.instantiate()
	rayo = RAYO.instantiate()
	
	if !disparar_rayo:
		add_child(aviso)
		aviso.global_position = global_position
		aviso.global_rotation = global_rotation
	if disparar_rayo:
		add_child(rayo)
		rayo.global_position = global_position
		rayo.global_rotation = global_rotation

func _on_teimpo_permanencia_aviso_timeout():
	#if rayo_activo && get_parent().get_parent().laser:
	#	disparar_rayo = false
	#	rayo.queue_free()
	if num_temporizador == 2:
		aviso.queue_free()
		disparar_rayo = true
		%AudioManager.lanzallamas.play()
		disparar()
		$"../../Tiempo permanencia rayo".paused = false
		$"../../Teimpo permanencia aviso".paused = true
		num_temporizador = 3

func _on_velocidad_aviso_timeout():
	#if get_parent().get_parent().laser:
	#	disparar()
	#	disparar_rayo = true
	#	rayo_activo = true
	#pass
	if num_temporizador == 1:
		disparar_rayo = false
		disparar()
		$"../../Velocidad aviso".paused = true
		$"../../Teimpo permanencia aviso".paused = false
		num_temporizador = 2

func _on_tiempo_permanencia_rayo_timeout():
	if num_temporizador == 3:
		rayo.queue_free()
		num_temporizador = 1
		$"../../Tiempo permanencia rayo".paused = true
		$"../../Velocidad aviso".paused = false
