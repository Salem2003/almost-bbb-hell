extends CharacterBody2D

var cuerpo_roto = preload("res://Assets/Bosses/Ahju/Ahju_roto.tscn").instantiate()
var ya_aparecio: bool = false

func _physics_process(_delta):
	if $"Mascara Ahju".vida <= 0:
		$Cosa.laser = true
		$"Cañones centrales superiores".rapido = true
	if $Cosa.vida <= 0:
		cuerpo_roto.global_position = global_position
		get_parent().add_child(cuerpo_roto)
		get_parent().get_parent().disparo = false
		queue_free()
