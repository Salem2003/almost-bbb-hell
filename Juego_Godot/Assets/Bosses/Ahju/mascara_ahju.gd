extends CharacterBody2D

var vida = 200
var sujeto = 0
var random = RandomNumberGenerator.new()
var num: int = 0
@onready var cura = preload("res://Assets/PJ/cura.tscn")
var curacion

func _ready():
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida

func recibir_daño(daño):
	vida -= daño
	%ProgressBar.value = vida
	if vida <= 150 && sujeto == 0:
		Spawnear()
		$Mascara1.visible = false
		$Mascara2.visible = true
		sujeto += 1
	if vida <= 100 && sujeto == 1:
		Spawnear()
		$Mascara2.visible = false
		$Mascara3.visible = true
		sujeto += 1
	if vida <= 50 && sujeto == 2:
		Spawnear()
		$Mascara3.visible = false
		$Mascara4.visible = true
		sujeto += 1
	if vida <= 0 && sujeto == 3:
		Spawnear()
		%AudioManager.mascara_rota_ahju.play()
		$CollisionShape2D.queue_free()
		$Mascara4.visible = false
		$Fuegos.visible = true
		%ProgressBar.queue_free()

func Spawnear():
	num = random.randi_range(0, 6)
	if num == 1:
		curacion = cura.instantiate()
		add_child(curacion)
		curacion.global_position = global_position
