extends CharacterBody2D

var vida = 45
@export var velocidad_rotacion: float
var random = RandomNumberGenerator.new()
var num: int = 0
@onready var cura = preload("res://Assets/PJ/cura.tscn")
var curacion

func _ready():
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida

func _physics_process(delta):
	rotation -= velocidad_rotacion * delta

func recibir_daño(daño):
	vida -= daño
	%ProgressBar.value = vida
	if vida <= 0:
		num = random.randi_range(0, 6)
		if num == 1:
			curacion = cura.instantiate()
			add_child(curacion)
			curacion.global_position = global_position
		$"Velocidad disparo".paused = true
		%Sprite.queue_free()
		%Colision.queue_free()
		%ProgressBar.queue_free()
		%"Velocidad disparo".paused = true
