extends Marker2D

const BALA = preload("res://Assets/Bosses/Ahju/proyectil_chico_1.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala

func disparar():
	bala = BALA.instantiate()
	$"CañonChico/Marker2D".add_child(bala)
	bala.global_position = $"CañonChico/Marker2D".global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	disparar()
