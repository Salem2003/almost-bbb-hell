extends Marker2D

const BALA_DERECHA = preload("res://Assets/Bosses/Ahju/proyectil_chico_2.tscn") #la convencion dice q las constantes se escriben en mayusculas
const BALA_IZQUIERDA = preload("res://Assets/Bosses/Ahju/proyectil_chico_2.tscn")
var entrar: bool = true

func _ready():
	%"Velocidad disparo".paused = true

func _physics_process(_delta):
	if get_parent().get_parent().get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		%"Velocidad disparo".paused = false
		entrar = false

func disparar():
	var bala_derecha = BALA_DERECHA.instantiate()
	var bala_izquierda = BALA_IZQUIERDA.instantiate()
	
	%Marker2D.add_child(bala_derecha)
	%Marker2D2.add_child(bala_izquierda)
	bala_derecha.position = %Marker2D2.global_position
	bala_derecha.rotation = %Marker2D2.global_rotation
	#get_node("/root/Game/Jefe 1/Cañon grande/Colision cañon grande/Pivot cañon grande/Disparador").add_child(bala_derecha)
	#get_node("/root/Game").add_child(bala_derecha)
	bala_izquierda.global_position = %Marker2D.global_position
	bala_izquierda.global_rotation = %Marker2D2.global_rotation
	
	


func _on_velocidad_disparo_timeout():
	%AudioManager.proyectil_chico_2.play()
	disparar()
