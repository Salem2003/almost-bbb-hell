extends Node2D

@export var velocidad_rotacion = 1.0

const BALA = preload("res://Assets/Bosses/Ahju/proyectil_chico_1.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala

func _physics_process(delta):
	rotation -= velocidad_rotacion * delta

func disparar():
	bala = BALA.instantiate()
	%Marker2D.add_child(bala)
	bala.global_position = %Marker2D.global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	%AudioManager.proyectil_chico.play()
	disparar()
