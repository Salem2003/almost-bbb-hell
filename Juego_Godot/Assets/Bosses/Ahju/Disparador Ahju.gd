extends Marker2D

var resultado_resta_de_vectores = Vector2()
var proyectiles_disparados: int = 0
var rafaga: bool = false

var entrar: bool = false
var entrar2: bool = true

const BALA = preload("res://Assets/Bosses/Ahju/bola_de_fuego.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala

func _ready():
	$"../Velocidad disparo mas rapido".paused = true
	$"../Velocidad disparo rapido".paused = true
	$"../Velocidad disparo".paused = true

func _physics_process(_delta):
	if get_parent().get_parent().get_parent().get_parent().disparo && entrar2:
		$"../Velocidad disparo".paused = false
		#$"../Velocidad disparo rapido".paused = false
		entrar2 = false
	
	if !entrar && get_parent().rapido:
		$"../Velocidad disparo mas rapido".paused = false
		$"../Velocidad disparo".paused = true

func disparar():
	bala = BALA.instantiate()
	add_child(bala)
	bala.global_position = global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	if !get_parent().rapido:
		%AudioManager.bola_de_fuego.play()
		disparar()
		proyectiles_disparados = 0
		rafaga = true
		$"../Velocidad disparo".paused = true
		$"../Velocidad disparo rapido".paused = false


func _on_velocidad_disparo_rapido_timeout():
	if proyectiles_disparados < 2 && rafaga && !get_parent().rapido:
		if proyectiles_disparados == 1:
			rafaga = false
			$"../Velocidad disparo".paused = false
		proyectiles_disparados += 1
		%AudioManager.bola_de_fuego.play()
		disparar()
	if proyectiles_disparados < 3 && rafaga && get_parent().rapido:
		if proyectiles_disparados == 2:
			rafaga = false
			$"../Velocidad disparo mas rapido".paused = false
		proyectiles_disparados += 1
		disparar()
		$"../Velocidad disparo rapido".paused = true


func _on_velocidad_disparo_mas_rapido_timeout():
	if get_parent().rapido:
		%AudioManager.bola_de_fuego.play()
		disparar()
		proyectiles_disparados = 0
		rafaga = true
		$"../Velocidad disparo mas rapido".paused = true
		$"../Velocidad disparo rapido".paused = false
