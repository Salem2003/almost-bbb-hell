extends Node2D

var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion = 0
@onready var objetivo = get_node("/root/Game/PJ/Jugador")
var entrar: bool = true

const BALA = preload("res://Assets/Bosses/Ahju/proyectil_mediano.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala

func _ready():
	$"../Velocidad disparo".paused = true

func _physics_process(delta):
	if get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		$"../Velocidad disparo".paused = false
		entrar = false
	
	resultado_resta_de_vectores = objetivo.position - global_position #esta resta da como resultado la posicion del PJ
	rotation = lerp_angle(rotation, atan2(resultado_resta_de_vectores.y, resultado_resta_de_vectores.x), delta * velocidad_rotacion)

func disparar():
	bala = BALA.instantiate()
	%Disparador.add_child(bala)
	bala.global_position = %Disparador.global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	%AudioManager.proyectil_mediano.play()
	disparar()
