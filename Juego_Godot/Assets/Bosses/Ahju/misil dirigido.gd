extends Area2D

@export var velocidad_proyectil = 0
var distancia_recorrida = 0
var distancia_limite = 2000
@export var daño_misil = 0
@export var vida: int = 0
@onready var objetivo = get_node("/root/Game/Jugador")

func _physics_process(delta):
	var direccion = Vector2.RIGHT.rotated(rotation) #hace q los proyectiles salgan disparados con la rotacion correcta
	#position += direccion * velocidad_proyectil * delta
	distancia_recorrida += velocidad_proyectil * delta
	
	if global_position < objetivo.global_position:
		global_position += Vector2(velocidad_proyectil, velocidad_proyectil)
	if global_position > objetivo.global_position:
		global_position -= Vector2(velocidad_proyectil, velocidad_proyectil)
	
	if distancia_recorrida >= distancia_limite:
		queue_free() #destruye el objeto

func _on_body_entered(body):
	if body.has_method("recibir_daño"):
		body.recibir_daño(daño_misil)
	queue_free()

func recibir_daño(daño):
	vida -= daño
	if vida <= 0:
		queue_free()
