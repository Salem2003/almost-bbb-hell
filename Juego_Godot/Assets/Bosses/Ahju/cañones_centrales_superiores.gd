extends Marker2D

@export var velocidad_rotacion: float
var rapido: bool = false

func _ready():
	$"Velocidad disparo".paused = true
	$"Velocidad disparo rapido".paused = true
	$"Velocidad disparo mas rapido".paused = true

func _physics_process(delta):
	rotation += velocidad_rotacion * delta
