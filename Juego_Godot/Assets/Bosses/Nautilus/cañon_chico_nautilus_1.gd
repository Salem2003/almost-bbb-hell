extends Node2D

var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion = 1.0


const BALA = preload("res://Assets/Bosses/Nautilus/proyectil_chico.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala

@export var horario: bool = false
var entrar: bool = true
var despausar: bool = false
var proyectiles_disparados: int
var rafaga: bool = false

func _ready():
	%"Velocidad disparo".paused = true
	$"Velocidad disparo rapido".paused = true

func _physics_process(delta):
	if get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		%"Velocidad disparo".paused = false
		entrar = false
	
	if horario:
		rotation -= velocidad_rotacion * delta
	if horario == false:
		rotation += velocidad_rotacion * delta

func disparar():
	bala = BALA.instantiate()
	%Disparador.add_child(bala)
	bala.global_position = %Disparador.global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	despausar = true
	%AudioManager.agua_chico.play()
	disparar()
	proyectiles_disparados = 0
	%"Velocidad disparo".paused = true
	$"Velocidad disparo rapido".paused = false
	rafaga = true

func _on_rotacion_horario_timeout():
	if horario:
		horario = false
	$"Rotacion horario".paused = true
	$"Rotacion antihorario".paused = false

func _on_rotacion_antihorario_timeout():
	if horario == false:
		horario = true
	$"Rotacion horario".paused = false
	$"Rotacion antihorario".paused = true


func _on_velocidad_disparo_rapido_timeout():
	if proyectiles_disparados < 9 && rafaga:
		if proyectiles_disparados == 8:
			rafaga = false
			$"Velocidad disparo rapido".paused = true
			%"Velocidad disparo".paused = false
		proyectiles_disparados += 1
		%AudioManager.agua_chico.play()
		disparar()
