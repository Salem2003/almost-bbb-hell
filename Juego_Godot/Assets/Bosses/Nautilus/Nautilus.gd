extends CharacterBody2D

var cuerpo_roto = preload("res://Assets/Bosses/Nautilus/Nautilus_roto.tscn").instantiate()
var ya_aparecio: bool = false

func _physics_process(_delta):
	if $Mascara.vida <= 0:
		$Ojo.laser = true
		$"Cañon grande".rapido = true
	if $Ojo.vida <= 0:
		cuerpo_roto.global_position = global_position
		get_parent().add_child(cuerpo_roto)
		get_parent().get_parent().disparo = false
		get_parent().remove_child($".")
