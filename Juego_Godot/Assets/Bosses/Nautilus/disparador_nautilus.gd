extends Marker2D

var objetivo
var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion: int

var proyectiles_disparados: int = 0
var rafaga: bool = false

var entrar: bool = false
var entrar2: bool = true

const BALA = preload("res://Assets/Bosses/Nautilus/proyectil_grande.tscn") #la convencion dice q las constantes se escriben en mayusculas
var bala

func _ready():
	$"../../Velocidad disparo mas rapido".paused = true
	$"../../Velocidad disparo rapido".paused = true
	$"../../Velocidad disparo".paused = true
	objetivo = get_node("/root/Game/PJ/Jugador")

func _physics_process(delta):
	resultado_resta_de_vectores = objetivo.position - global_position #esta resta da como resultado la posicion del PJ
	rotation = lerp_angle(rotation, atan2(resultado_resta_de_vectores.y, resultado_resta_de_vectores.x), delta * velocidad_rotacion)
	
	if get_parent().get_parent().get_parent().get_parent().get_parent().disparo && entrar2:
		#$"../../Velocidad disparo rapido".paused = false
		$"../../Velocidad disparo".paused = false
		entrar2 = false
	
	if !entrar && get_parent().get_parent().rapido:
		$"../../Velocidad disparo mas rapido".paused = false
		#$"../../Velocidad disparo".paused = true

func disparar():
	bala = BALA.instantiate()
	add_child(bala)
	bala.global_position = global_position
	bala.global_rotation = global_rotation

func _on_velocidad_disparo_timeout():
	get_parent().despausar = true
	if !get_parent().get_parent().rapido:
		%AudioManager.agua_grande.play()
		disparar()
		proyectiles_disparados = 0
		rafaga = true
		$"../../Velocidad disparo".paused = true
		$"../../Velocidad disparo rapido".paused = false

func _on_velocidad_disparo_rapido_timeout():
	if proyectiles_disparados < 7 && rafaga:
		if proyectiles_disparados == 6:
			rafaga = false
			$"../../Velocidad disparo rapido".paused = true
			if !get_parent().get_parent().rapido:
				$"../../Velocidad disparo".paused = false
			elif get_parent().get_parent().rapido:
				$"../../Velocidad disparo mas rapido".paused = false
		proyectiles_disparados += 1
		%AudioManager.agua_grande.play()
		disparar()


func _on_velocidad_disparo_mas_rapido_timeout():
	if get_parent().get_parent().rapido:
		%AudioManager.agua_grande.play()
		disparar()
		proyectiles_disparados = 0
		rafaga = true
		$"../../Velocidad disparo mas rapido".paused = true
		$"../../Velocidad disparo rapido".paused = false
