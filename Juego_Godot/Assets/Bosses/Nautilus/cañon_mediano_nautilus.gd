extends Node2D

var objetivo
var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion: int
var proyectiles_disparados: int
var rafaga: bool = false
var entrar: bool = true
var despausar: bool = false

const BALA_DERECHA = preload("res://Assets/Bosses/Nautilus/proyectil_mediano.tscn") #la convencion dice q las constantes se escriben en mayusculas
const BALA_IZQUIERDA = preload("res://Assets/Bosses/Nautilus/proyectil_mediano.tscn")

func _ready():
	objetivo = get_node("/root/Game/PJ/Jugador")
	%"Velocidad disparo".paused = true
	%"Velocidad disparo rapido".paused = true

func _physics_process(delta):
	if get_parent().get_parent().get_parent().get_parent().disparo && entrar:
		%"Velocidad disparo".paused = false
		#$"../Velocidad disparo rapido".paused = false
		entrar = false
	
	resultado_resta_de_vectores = objetivo.position - global_position #esta resta da como resultado la posicion del PJ
	rotation = lerp_angle(rotation, atan2(resultado_resta_de_vectores.y, resultado_resta_de_vectores.x), delta * velocidad_rotacion)

func disparar():
	var bala_derecha = BALA_DERECHA.instantiate()
	var bala_izquierda = BALA_IZQUIERDA.instantiate()
	
	%Disparador.add_child(bala_derecha)
	%Disparador2.add_child(bala_izquierda)
	bala_derecha.position = %Disparador2.global_position
	bala_derecha.rotation = %Disparador2.global_rotation
	bala_izquierda.global_position = %Disparador.global_position
	bala_izquierda.global_rotation = %Disparador2.global_rotation

func _on_velocidad_disparo_timeout():
	despausar = true
	%AudioManager.agua_mediano.play()
	disparar()
	proyectiles_disparados = 0
	%"Velocidad disparo".paused = true
	%"Velocidad disparo rapido".paused = false
	rafaga = true


func _on_velocidad_disparo_rapido_timeout():
	if proyectiles_disparados < 4 && rafaga:
		if proyectiles_disparados == 3:
			rafaga = false
			%"Velocidad disparo rapido".paused = true
			%"Velocidad disparo".paused = false
		proyectiles_disparados += 1
		%AudioManager.agua_mediano.play()
		disparar()
