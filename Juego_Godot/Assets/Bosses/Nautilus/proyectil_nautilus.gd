extends Area2D

@export var velocidad_proyectil = 0
var velocidad: int = 0
var distancia_recorrida = 0
var distancia_limite = 2000
@export var daño = 0
@export var aumento_velocidad: int

func _ready():
	%"Aumento velocidad".paused = true

func _physics_process(delta):
	var direccion = Vector2.RIGHT.rotated(rotation) #hace q los proyectiles salgan disparados con la rotacion correcta
	position += direccion * velocidad * delta
	distancia_recorrida += velocidad * delta
	
	if get_parent().get_parent().despausar:
		%Quietud.paused = false
		%"Aumento velocidad".paused = true
	
	if distancia_recorrida >= distancia_limite:
		queue_free() #destruye el objeto

func _on_body_entered(body):
	if body.has_method("recibir_daño"):
		body.recibir_daño(daño)
	queue_free()


func _on_aumento_velocidad_timeout():
	velocidad += aumento_velocidad


func _on_quietud_timeout():
	get_parent().get_parent().despausar = false
	%"Aumento velocidad".paused = false
	%Quietud.paused = true
	velocidad = velocidad_proyectil
