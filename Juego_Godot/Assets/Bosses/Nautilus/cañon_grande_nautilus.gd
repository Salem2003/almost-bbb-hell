extends Node2D

var resultado_resta_de_vectores = Vector2()
@export var velocidad_rotacion = 0
@onready var objetivo = get_node("/root/Game/PJ/Jugador")
var entrar: bool = true

var despausar: bool = false

func _physics_process(delta):
	resultado_resta_de_vectores = objetivo.position - global_position #esta resta da como resultado la posicion del PJ
	rotation = lerp_angle(rotation, atan2(resultado_resta_de_vectores.y, resultado_resta_de_vectores.x), delta * velocidad_rotacion)
