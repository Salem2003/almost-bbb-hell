extends Area2D

@export var daño: int

func _on_body_entered(body):
	if body.has_method("burbujear"):
		if !body.burbujeado:
			body.burbujear()
	get_parent().queue_free()
