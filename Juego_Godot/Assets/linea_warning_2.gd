extends Sprite2D

var velocidad_movimiento: int = 200

func _physics_process(delta):
	if visible:
		position.x -= velocidad_movimiento * delta
