extends CharacterBody2D

@export var vida: int
var vida_max: int = 0
var entrar: bool =true
signal sin_vida #asi se crean señales personalizadasget_node("/root/Game/Jugador")
@onready var burbuja = preload("res://Assets/Bosses/Nautilus/burbujeado.tscn")
var burbujin
var burbujeado: bool = false
var aceleracion: int = 600
var aceleracion_aux: int = 600
@export var daño_proyectil: int
var revivir: bool = false

func _ready():
	vida_max = vida
	%ProgressBar.max_value = vida
	%ProgressBar.value = vida
	$"Velocidad disparo".paused = true
	%Burbuja.paused = true

func _physics_process(_delta): #physics update 
	if get_parent().get_parent().disparo && entrar:
		$"Velocidad disparo".paused = false
		entrar = false
	if !get_parent().get_parent().disparo:
		$"Velocidad disparo".paused = true
		entrar = true
	if burbujeado:
		aceleracion = 300
	elif !burbujeado:
		aceleracion = 600
	movement()

func movement():
	if !get_parent().get_parent().carta:
		var direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
		velocity = direction * aceleracion # si no hay input direction es = a 0
		move_and_slide() #este metodo aplica delta automaticamente
	if position.x <= -500:
		position.x = -500
	if position.x >= 500:
		position.x = 500
	if position.y <= -380:
		position.y = -380
	if position.y >= 490: #380 + 110
		position.y = 490
	
	#esto es para referenciar un nodo hijo, y en este caso llamar a una funcion en su script para reproducir una animacion de caminar
	#if velocity.lenght() > 0.0:
	#get node no recibe el nombre del nodo como parametro, sino q recibe su ruta, es decir q si Pj fuera hijo de CollisionShape 2D
	# el parametro a escribir seria su ruta siendo ("CollisionShape2D/Pj")
		#get_node("Pj").play_walk_animation
	#else:
		#get_node("Pj").play_idle_animation
		#$Pj.play_walk_animation() es lo mismo q get_node("Pj").play_walk_animation()
		#si maarco el nodo como unico tambien puedo escribir %Pj sin importar su ubicacion en la jerarquia, pero solo funciona con la escena en la q se encuentra

func disparar():
	const BALA_DERECHA = preload("res://Assets/PJ/bala_pj_derecha.tscn") #la convencion dice q las constantes se escriben en mayusculas
	const BALA_IZQUIERDA = preload("res://Assets/PJ/bala_pj_derecha.tscn")
	var bala_derecha = BALA_DERECHA.instantiate()
	var bala_izquierda = BALA_IZQUIERDA.instantiate()
	
	bala_derecha.global_position = %Disparador2.global_position
	bala_derecha.global_rotation = %Disparador2.global_rotation
	%Disparador2.add_child(bala_derecha)
	bala_izquierda.global_position = %Disparador.global_position
	bala_izquierda.global_rotation = %Disparador.global_rotation
	%Disparador.add_child(bala_izquierda)

func _on_velocidad_disparo_timeout():
	$AudioManager.proyectil_pj.play()
	disparar()

func recibir_daño(daño):
	vida -= daño
	if vida <= 0 && !revivir:
		sin_vida.emit()
	elif vida <= 0 && revivir:
		revivir = false
		vida = vida_max
		$AudioManager.revivir.play()
	%ProgressBar.value = vida

func burbujear():
	$"Velocidad disparo".paused = true
	burbujeado = true
	%Burbuja.paused = false
	burbujin = burbuja.instantiate()
	add_child(burbujin)


func _on_burbuja_timeout():
	$"Velocidad disparo".paused = false
	burbujeado = false
	burbujin.visible = false
	%Burbuja.paused = true

func Curar(cura):
	if vida + cura >= vida_max:
		vida = vida_max
	else:
		vida += cura
	%ProgressBar.value = vida
