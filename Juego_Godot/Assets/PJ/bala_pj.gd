extends Area2D

var velocidad_proyectil = 2000
var distancia_recorrida = 0
var distancia_limite = 2000
var daño: int

func _physics_process(delta):
	daño = get_parent().get_parent().get_parent().daño_proyectil
	
	var direccion = Vector2.UP.rotated(rotation) #hace q los proyectiles salgan disparados con la rotacion correcta
	position += direccion * velocidad_proyectil * delta
	distancia_recorrida += velocidad_proyectil * delta
	
	if distancia_recorrida >= distancia_limite: #destruyo la bala si viajo cierta distancia
		queue_free() #destruye el objeto



func _on_body_entered(body): #body hace referencia a un CharacterBody2D
	if body.has_method("recibir_daño"):
		body.recibir_daño(daño)
	queue_free()
	#SingletonRecibirDmg.recibir_daño(daño, 0, 0, 0, 1)
