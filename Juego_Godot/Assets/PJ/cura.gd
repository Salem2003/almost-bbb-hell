extends Area2D

var vida: int = 40
var distancia_recorrida = 0
var distancia_limite = 2000
var velocidad_movimiento: int = 200

func _physics_process(delta):
	position.y += velocidad_movimiento * delta
	distancia_recorrida += velocidad_movimiento * delta
	
	if distancia_recorrida >= distancia_limite:
		queue_free() #destruye el objeto

func _on_body_entered(body):
	if body.has_method("Curar"):
		body.Curar(vida)
		queue_free()
