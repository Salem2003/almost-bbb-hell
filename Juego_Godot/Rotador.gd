extends Marker2D

@export var velocidad_rotacion: float = 0

func _physics_process(delta):
	rotation += velocidad_rotacion * delta
